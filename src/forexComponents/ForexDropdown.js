import React , { Component } from 'react';
import { 
	FormGroup,
	Label,
	DropdownToggle,
	DropdownMenu,
	Dropdown,
	DropdownItem
	} from 'reactstrap';

import Currencies from './ForexData';

class ForexDropdown extends Component {
	state={
		dropdownIsOpen : false,
		currency : null
	}
	render(){
		return(
			<React.Fragment>
				<FormGroup>
					<Label className="py-2 w-100">{this.props.label}</Label>

					<Dropdown
						isOpen= { this.state.dropdownIsOpen }
						toggle=	{ ()=>{ this.setState({dropdownIsOpen : !this.state.dropdownIsOpen}) } }

					>

						<DropdownToggle
						caret
						className="px-3 w-100">
							{ this.state.currency == null 
								? "Choose Currency" 
								: this.state.currency.code + " - " + this.state.currency.currency }
						</DropdownToggle>

						<DropdownMenu
							className="w-100"
						>

							{ Currencies.map((currency,index)=>(
								<DropdownItem 
								key={ index }
								className="py-2"
								onClick={ ()=>{
									this.props.handleCurrency(currency)
									this.setState({currency})
								}}
								>
									{ currency.code } - { currency.currency }
								</DropdownItem>
							)) }

						</DropdownMenu>

					</Dropdown>

				</FormGroup>
			</React.Fragment>
		)
	}
}

export default ForexDropdown;
