import React , { Component } from 'react';
import ForexDropdown from './ForexDropdown';
import ForexInput from './ForexInput';
import { Button,Input,Label } from 'reactstrap';
import Rates from './Rates';


class Forex extends Component {
	state={
		amount : 0,
		base : "",
		target : "",
		convertedAmmount : 0,
		rates : "",
	}

	handleAmount = e => {
		this.setState({amount : e.target.value})
	}

	handleBase = currency => {
		const code = this.state.base;
		this.setState({base:currency.code});
		fetch('https://api.exchangeratesapi.io/latest?base='+code).then(res=>res.json()).then(res=>{
					this.setState({rates:res.rates});
				})
	}

	handleTarget = currency => {
		this.setState({target:currency.code})
	}

	handleConvert = () => {

		if(this.state.target === "" || this.state.base === ""){
			alert("Please provide details");
		}else{
			if(this.state.amount <= 0){
				alert("Invalid amount")
			}else{
				const code = this.state.base;
				const targetCode = this.state.target;
				fetch('https://api.exchangeratesapi.io/latest?base='+code).then(res=>res.json()).then(res=>{

					const target = res.rates[targetCode];
					const convertedAmmount = this.state.amount * target;

					this.setState({convertedAmmount});

				})
			}
		}

	}

	render() {
		return(
			<React.Fragment>
				<div className="text-center mt-5">
					<h1> Forex Calculator </h1>
				</div>

				<div className="row">
					<div className="col-lg-12">
						<div className="row">
							<div className="col-lg-6 ">
								<ForexDropdown 
								label = { 'Base Currency' }
								handleCurrency={this.handleBase}
								/>
							</div>
							<div className="col-lg-6">
								<ForexDropdown 
								label = { 'Target Currency' }
								handleCurrency={this.handleTarget}
								/>
							</div>
						</div>
					</div>
				</div>

				<div className="row">
					<div className="col-lg-12">
						<div className="row">

							<div className="col-lg-6">
								<ForexInput 
									placeholder={ 'Amount to convert' }
									label={ 'Amount' }
									onChange={ this.handleAmount }

								/>
								<Button
								color="info"
								onClick={ this.handleConvert }
								>
									Convert
								</Button>
							</div>

							<div className="col-lg-6">
								<Label>Result</Label>
								<Input 
									disabled
									type="number"
									placeholder={ this.state.convertedAmmount }
								/>
							</div>

						</div>
					</div>
				</div>

				<div className="row">
					<div className="col-lg-12">
						<div className="d-flex flex-wrap w-100 align-items-center justify-content-around mt-3">
							<Rates rates={ this.state.rates }/>
						</div>
					</div>
				</div>

			</React.Fragment>
		)
	}
}

export default Forex;