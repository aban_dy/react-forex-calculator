import React , { Component } from 'react';

class Rates extends Component{
	render(){
		const { rates } = this.props;
		return(
			<React.Fragment>
			{
				Object.keys(rates).map(key=>(
						<span className="badge badge-primary  px-2 m-2" key={ key }>
							{ key }  -  { rates[key] } 
						</span>
					))
			}
			
			</React.Fragment>
		)
	}
}

export default Rates;