import React , { Component } from 'react';
import Forex from './forexComponents/Forex';

class ForexApp extends Component {
	render() {
		return(
			<React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="row">
              	<div className ="col-lg-10 offset-lg-1">
              		<Forex />
              	</div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
		)
	}
}

export default ForexApp;