import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
// import ForexApp from './ForexApp';
import CalculatorApp from './Calculator/CalculatorApp';

// import Calc from './Calc/Calc';

import * as serviceWorker from './serviceWorker';


ReactDOM.render(
  <React.StrictMode>
  	<div className="bg-dark vh-100">
  		<CalculatorApp />
  	</div>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
