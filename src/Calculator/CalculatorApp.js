import React,{ useState } from 'react';
import ButtonCirlce from './calcComponents/Button';

const CalculatorApp = () => {

	const [ input , setInput ] = useState(""); 
	const [ prevNumber , setprevNumber ] = useState(""); 
	let [ currentNumber, setCurrentNumber ] = useState(""); 
	const [ operator , setOperator ] = useState("");
	const [ operatorText , setOperatorText ] = useState ("")

	const addToInput = val => {
		setInput( input + val );
	}

	const addZero = val => {
		if(input !== ""){
			setInput( input + val );
		}
	}

	const addDecimal = val => {
		if(input.indexOf(".") === -1){
			setInput(input + val);
		}
	}

	/*
		Operations
	*/

	const equals = () => {
		currentNumber = input;

		if(operator === "plus"){
			let result = parseFloat(prevNumber) + parseFloat(currentNumber);
			let res = result.toString();
			setInput(res);
			setprevNumber("");
			setOperatorText("");
		}
		else if(operator === "subtract"){
			let result = parseFloat(prevNumber) - parseFloat(currentNumber);
			let res = result.toString();
			setInput(res);
			setprevNumber("");
			setOperatorText("");
		}
		else if(operator === "divide"){
			let result = parseFloat(prevNumber) / parseFloat(currentNumber);
			let res = result.toString();
			setInput(res);
			setprevNumber("");
			setOperatorText("");
		}
		else if(operator === "multiply"){
			let result = parseFloat(prevNumber) * parseFloat(currentNumber);
			let res = result.toString();
			setInput(res);
			setprevNumber("");
			setOperatorText("");
		}
	}

	const add = (val) => {
		if(prevNumber === ""){
			setprevNumber(input);
			setInput("");
			setOperator("plus");
			setOperatorText(val)
		}else{
			setOperatorText(val);
			setOperator("plus");
		}
	}

	const subtract = (val) => {
		if(prevNumber === ""){
			setprevNumber(input);
			setInput("");
			setOperator("subtract");
			setOperatorText(val);
		}else{
			setOperatorText(val);
			setOperator("subtract");
		}
	}

	const divide = (val) => {
		if(prevNumber === ""){
			setprevNumber(input);
			setInput("");
			setOperator("divide");
			setOperatorText(val);
		}else{
			setOperatorText(val);
			setOperator("divide");
		}
	}

	const multiply = (val) => {
		if(prevNumber === ""){
			setprevNumber(input);
			setInput("");
			setOperator("multiply");
			setOperatorText(val);
		}else{
			setOperatorText(val);
			setOperator("multiply");
		}
	}

	const ac = () => {
		setInput("");
		setOperatorText("");
		setprevNumber("");
	}


	const clear = ()=> {
		let str = input;
		setInput(str.slice(0,-1));
	}

	return(
			<React.Fragment>
				<div className="container d-flex align-items-center justify-content-center">
					<div className="row">
						<div className="col-lg-12 col-md-12 col-sm-12">

								<div className="mt-4"
								style={{
									width : '400px',
									height : '600px',
									borderRadius : '12px'
								}}
								>
									<div className="row">
										<div className="col-lg-12">
											<div className="container">
	
												<div className="row mt-5 p-3">
													<div className="col-lg-12">
														<div className="text-left">
															<h5 className="text-info">
																{ operatorText === "" ? (<h5>&nbsp;</h5>) : operatorText }
															</h5>
														</div>
														<div className="text-right">
															<h5 className="text-secondary">
																{ prevNumber === "" ? (<h5>&nbsp;</h5>) : prevNumber }
															</h5>
															<strong className="text-primary h3">
																{ input === "" ? "0" : input }
															</strong>
														</div>
													</div>
												</div>
												<div className="row">
													<ButtonCirlce text={ "7" } onClick={addToInput} />
													<ButtonCirlce text={ "8" } onClick={addToInput}/>
													<ButtonCirlce text={ "9" } onClick={addToInput}/>
													<ButtonCirlce text={ "/" } onClick={divide} color={ '#bd224b' } />
												</div>
												<div className="row">
													<ButtonCirlce text={ "4" } onClick={addToInput}/>
													<ButtonCirlce text={ "5" } onClick={addToInput}/>
													<ButtonCirlce text={ "6" } onClick={addToInput}/>
													<ButtonCirlce text={ "x" } onClick={multiply} color={ '#bd224b' } />
												</div>
												<div className="row">
													<ButtonCirlce text={ "1" } onClick={addToInput}/>
													<ButtonCirlce text={ "2" } onClick={addToInput}/>
													<ButtonCirlce text={ "3" } onClick={addToInput}/>
													<ButtonCirlce text={ "+" } onClick={add} color={ '#bd224b' }/>
												</div>
												<div className="row">
													<ButtonCirlce text={ "." } onClick={addDecimal} />
													<ButtonCirlce text={ "0" } onClick={addZero}/>
													<ButtonCirlce text={ "=" } onClick={equals} color={ '#bd224b' }/>
													<ButtonCirlce text={ "-" } onClick={subtract} color={ '#bd224b' }/>
												</div>
												<div className="row p-3">
													<button 
													className="btn btn-dark py-2"
													style={{
														width : "45%",
														borderRadius : "24px"
													}}
													onClick={clear}
													>C</button>

													<button 
													className="btn btn-dark py-2 ml-auto"
													style={{
														width : "45%",
														borderRadius : "24px"
													}}
													onClick={ac}
													>AC</button>

												</div>
											</div>
										</div>
									</div>

								</div>

						</div>
					</div>
				</div>
			</React.Fragment>
		)
}

export default CalculatorApp;