import React from 'react';


const ButtonCircle = props => {

	return(

			<React.Fragment>
				<div className="col-lg-3 col-md-3 col-sm-3 p-3">
					<button className="btn btn-dark rounded-circle"
					style={{
						height : "64px",
						width : "64px",
						backgroundColor : props.color
					}}
					onClick={ ()=>{ props.onClick(props.text) } }
					>
						{ props.text }
					</button>
				</div>
			</React.Fragment>
		)
}

export default ButtonCircle;