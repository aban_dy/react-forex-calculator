import React from 'react';

const Button = props => {
	return(
			<React.Fragment>
				<button
					className="btn btn-secondary"
					style={{
						height : "64px",
						width : "64px",
						backgroundColor : props.color
					}}
					onClick = { ()=>{ props.onClick(props.text) } }
				>
					<span className="text-white">
						{ props.text }
					</span>
				</button>
			</React.Fragment>
		)
}

export default Button;