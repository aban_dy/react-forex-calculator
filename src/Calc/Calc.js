import React , { useState } from 'react';
import Button from './Button';

const Calc = () => {

	/*
		States
	*/
	const [ input , setInput ] = useState("");
	const [ previousNumber , setPreviousNumber ] = useState("");
	let [ currentNumber , setCurrentNumber ] = useState("");
	const [ operator , setOperator ] = useState("");
	const [ operatorText , setOperatorText ] = useState(""); 

	/*
		Functionalities
	*/
	const addToInput = val => {
		setInput( input + val );
	}

	const addDecimal = val => {
		if(input.indexOf(".") === -1){
			setInput( input + val );
		}
	}

	const addZero = val => {
		if(input !== ""){
			setInput( input + val );
		}
	}

	const clear = () => {
		setInput("");
		setOperatorText("");
		setPreviousNumber("");
	}

	/*
		Operations
	*/
	const add = val => {
		setPreviousNumber(input);
		setInput("");
		setOperator("add");
		setOperatorText(val);
	}

	const divide = val => {
		setPreviousNumber(input);
		setInput("");
		setOperator("divide");
		setOperatorText(val);
	}

	const subtract = val => {
		setPreviousNumber(input);
		setInput("");
		setOperator("subtract");
		setOperatorText(val);
	}

	const multiply = val => {
		setPreviousNumber(input);
		setInput("");
		setOperator("multiply");
		setOperatorText(val);
	}

	/*
		Evaluation
	*/

	const equals = ()=> {

		currentNumber = input;

		if(operator === "add"){
			let result = parseFloat(currentNumber) + parseFloat(previousNumber);
			let res = result.toString();
			setInput(res);
		}
		else if(operator === "subtract"){
			let result = parseFloat(currentNumber) - parseFloat(previousNumber);
			let res = result.toString();
			setInput(res);
		}
		else if(operator === "multiply"){
			let result = parseFloat(currentNumber) * parseFloat(previousNumber);
			let res = result.toString();
			setInput(res);
		}
		else if(operator === "divide"){
			let result = parseFloat(currentNumber) / parseFloat(previousNumber);
			let res = result.toString();
			setInput(res);
		}

	}



	return(
		<React.Fragment>
			<div className="container">
				<div className="row">
					<div className="col-lg-12">
						<div className="row d-flex flex-row">
							<span className="text-white p-3">
								{ input === "" ? "0" : input } 
							</span>
						</div>

						<div className="row">
							<Button text={ '7' } color={ '#1b0238' } onClick={ addToInput }/>
							<Button text={ '8' } color={ '#1b0238' } onClick={ addToInput }/>
							<Button text={ '9' } color={ '#1b0238' } onClick={ addToInput }/>
							<Button text={ '/' } color={ '#80056f' } onClick={ divide }/>
						</div>
						<div className="row">
							<Button text={ '4' } color={ '#1b0238' } onClick={ addToInput }/>
							<Button text={ '5' } color={ '#1b0238' } onClick={ addToInput }/>
							<Button text={ '6' } color={ '#1b0238' } onClick={ addToInput }/>
							<Button text={ 'X' } color={ '#80056f' } onClick={ multiply }/>
						</div>
						<div className="row">
							<Button text={ '1' } color={ '#1b0238' } onClick={ addToInput }/>
							<Button text={ '2' } color={ '#1b0238' } onClick={ addToInput }/>
							<Button text={ '3' } color={ '#1b0238' } onClick={ addToInput }/>
							<Button text={ '+' } color={ '#80056f' } onClick={ add }/>
						</div>
						<div className="row">
							<Button text={ '.' } color={ '#1b0238' } onClick={ addDecimal }/>
							<Button text={ '0' } color={ '#1b0238' } onClick={ addZero }/>
							<Button text={ '=' } color={ '#1b0238' } onClick={ equals }/>
							<Button text={ '-' } color={ '#80056f' } onClick={ subtract }/>
						</div>
						<div className="row">
							<button
							className="btn btn-danger btn-sm"
							onClick={ clear }
							>
							Clear
							</button>
						</div>

					</div>
				</div>
			</div>
		</React.Fragment>
	)
}
export default Calc;